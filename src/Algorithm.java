//Student: Lars Esselink | s1058642
import java.util.*;

public class Algorithm {

    private final List<Pillar> pillars = new ArrayList<>();
    private final List<Disk> disks = new ArrayList<>();
    private int N;
    private int M;
    private int W;
    private final Pillar src = new Pillar(0,0, 0);
    private Pillar target;
    private final PriorityQueue<Pillar> queue = new PriorityQueue<>();
    private final LinkedHashSet<Pillar> visited = new LinkedHashSet<>();
    private final HashMap<Pillar, Pair<Disk,Long>> distance =  new HashMap<>();
    private final List<Pair<Pillar, Disk>> pairs = new ArrayList<>();




    public void main(){
        src.setCost(0L);

        readFromConsole();
        makePairs();


        algorithm();


        Pillar lastPillar = new Pillar(0,0,0);
        for(Pillar pillar: visited){
            lastPillar = pillar;
        }
        System.out.println(distance.get(lastPillar).getValue());

    }




    public void algorithm(){
        distance.put(src,new Pair<>(new Disk(0,0),0L));
        queue.add(src);

        for(Pair<Pillar, Disk> pair:pairs){
            distance.put(pair.getKey(),new Pair<>(pair.getValue(),Long.MAX_VALUE));
        }

        distance.put(target, new Pair<>(new Disk(0,0),Long.MAX_VALUE));


        src.setDisk(new Disk(0,0));

        while(!queue.isEmpty()){
            Pillar pillar1 = queue.remove();
            Disk disk1 = pillar1.getDisk();

            visited.add(pillar1);

            for(Pair<Pillar, Disk> pair : getAdj(pillar1,disk1)){
                Pillar pillar2 = pair.getKey();
                Disk disk2 = pair.getValue();




                long neighbourCost = distance.get(pillar2).getValue();
                long currentCost = distance.get(pillar1).getValue();
                long edgeWeight = disk2.getCost();
                long newCost = currentCost + edgeWeight;
                boolean shortestPath = neighbourCost > (currentCost + edgeWeight);

                if(shortestPath && pillar2 != target){
                    distance.put(pillar2,new Pair<>(disk2,newCost));
                    pillar2.setDisk(disk2);
                }

                if(!visited.contains(pillar2)){
                    queue.add(pillar2);
                }
            }

        }
    }

    public List<Pair<Pillar,Disk>> getAdj(Pillar pillar, Disk disk){
        List<Pair<Pillar,Disk>> temp = new ArrayList<>();

        for(Pair<Pillar, Disk> entry : pairs){
            Pillar pillarPair = entry.getKey();
            Disk diskPair = entry.getValue();

            if(distancePoints(pillar, pillarPair) <= diskPair.getRadius() + disk.getRadius() && pillar != pillarPair){
                temp.add(new Pair<>(pillarPair,diskPair));
            }

        }
        return temp;
    }

    public void makePairs (){
        for (int i = 1; i < pillars.size()-1; i++) {
            for (Disk disk : disks) {
                pairs.add(new Pair<>(pillars.get(i), disk));
            }
        }
        pairs.add(new Pair<>(target,new Disk(0,0)));
    }

    public double distancePoints(Pillar pillar1,Pillar pillar2) {
        double x1 = pillar1.getX();
        double y1 = pillar1.getY();
        double x2 = pillar2.getX();
        double y2 = pillar2.getY();
        return (long) Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
    }

    public void readFromConsole(){
        Scanner sc = new Scanner(System.in);;

        N = sc.nextInt();
        M = sc.nextInt();
        W = sc.nextInt();

        pillars.add(src);
        Pillar tempPillar;
        for(int i = 1; i < N + 1; i++){
            tempPillar = new Pillar(sc.nextInt(),sc.nextInt(),i);
            pillars.add(tempPillar);
        }
        target = new Pillar(0,W, -1);
        pillars.add(target);

        Disk tempDisk;
        for(int i = N + 1; i <= N + M; i++){
            tempDisk = new Disk(sc.nextInt(),sc.nextInt());
            disks.add(tempDisk);
        }

    }

}
