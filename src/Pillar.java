//Student: Lars Esselink | s1058642

public class Pillar implements Comparable<Pillar> {
    private final int X;
    private final int Y;
    private Long Cost = Long.MAX_VALUE;
    private final int ID;
    private Disk disk = new Disk(0, Long.MAX_VALUE);


    public Pillar(int X, int Y, int ID){
        this.X = X;
        this.Y = Y;
        this.ID = ID;
    }

    public int getY(){
        return Y;
    }

    public int getX(){
        return X;
    }

    public void setCost(Long cost){
        this.Cost = cost;
    }

    public Long getCost(){
        return Cost;
    }

    public int getID(){
        return ID;
    }

    public Disk getDisk(){
        return disk;
    }

    public void setDisk(Disk disk){
        this.disk = disk;
    }

    @Override
    public int compareTo(Pillar pillar) {
        return Long.compare(this.getCost(), pillar.getCost());
    }
}
