//Student: Lars Esselink | s1058642

public class Disk {

    private final long Cost;
    private final long Radius;

    public Disk(long Radius, long Cost){
        this.Radius = Radius;
        this.Cost = Cost;
    }

    public long getCost(){
        return Cost;
    }

    public long getRadius(){
        return Radius;
    }
}
